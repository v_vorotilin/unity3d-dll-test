﻿using System;
using System.Net;
using System.Reflection;
using UnityEngine;

public class Test : MonoBehaviour {

    // Use this for initialization
    void Start () {

    }

    private object _instance;
    private Type _type;

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.Space)) {
            WebClient client = new WebClient();

            byte[] assemblyBytes = client.DownloadData("https://dl.dropboxusercontent.com/u/15927363/AssemblyTest/Unity%20.net%203.5/TestAss.dll");
            byte[] debugSymbolsBytes = client.DownloadData("https://dl.dropboxusercontent.com/u/15927363/AssemblyTest/Unity%20.net%203.5/TestAss.dll.mdb");

            Assembly assembly = Assembly.Load(assemblyBytes, debugSymbolsBytes);
            _type = assembly.GetTypes()[0];

            _instance = Activator.CreateInstance(_type);

            Debug.Log("Assembly loaded");
        }

        if (Input.GetKeyDown(KeyCode.Return)) {
            Debug.Log("Invoking...");

            _type.GetMethod("Print2").Invoke(_instance, new object[] { 2 });

            Debug.Log("Invoked");
        }

    }
}
